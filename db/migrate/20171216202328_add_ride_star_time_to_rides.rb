class AddRideStarTimeToRides < ActiveRecord::Migration[5.1]
  def change
    add_column :rides, :ride_start_time, :datetime
  end
end
