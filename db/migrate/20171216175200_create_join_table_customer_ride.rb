class CreateJoinTableCustomerRide < ActiveRecord::Migration[5.1]
  def change
    create_join_table :customers, :rides do |t|
      t.index [:customer_id, :ride_id]
      t.index [:ride_id, :customer_id]
    end
  end
end
