class CreateJoinTableDriverRide < ActiveRecord::Migration[5.1]
  def change
    create_join_table :drivers, :rides do |t|
      t.index [:driver_id, :ride_id]
      t.index [:ride_id, :driver_id]
    end
  end
end
