class CreateCustomers < ActiveRecord::Migration[5.1]
  def change
    create_table :customers do |t|
      t.boolean :requested_ride, default: false

      t.timestamps
    end
  end
end
