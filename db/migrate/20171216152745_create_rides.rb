class CreateRides < ActiveRecord::Migration[5.1]
  def change
    create_table :rides do |t|
      t.timestamp :time
      t.string :location
      t.integer :status, default: 0

      t.timestamps
    end
  end
end
