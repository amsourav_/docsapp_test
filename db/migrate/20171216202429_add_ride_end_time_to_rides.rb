class AddRideEndTimeToRides < ActiveRecord::Migration[5.1]
  def change
    add_column :rides, :ride_end_time, :datetime
  end
end
