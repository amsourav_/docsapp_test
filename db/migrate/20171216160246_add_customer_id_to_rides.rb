class AddCustomerIdToRides < ActiveRecord::Migration[5.1]
  def change
    add_column :rides, :customer_id, :integer
  end
end
