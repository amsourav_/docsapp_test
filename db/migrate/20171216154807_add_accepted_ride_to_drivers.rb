class AddAcceptedRideToDrivers < ActiveRecord::Migration[5.1]
  def change
    add_column :drivers, :accepted_ride, :boolean, default: false
  end
end
