# == Schema Information
#
# Table name: drivers
#
#  id            :integer          not null, primary key
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  accepted_ride :boolean          default(FALSE)
#

class Driver < ApplicationRecord
    has_many :rides
end
