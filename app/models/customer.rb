# == Schema Information
#
# Table name: customers
#
#  id             :integer          not null, primary key
#  requested_ride :boolean          default(FALSE)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class Customer < ApplicationRecord
    has_many :rides
end
