# == Schema Information
#
# Table name: rides
#
#  id              :integer          not null, primary key
#  time            :datetime
#  location        :string
#  status          :integer          default("waiting")
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  driver_id       :integer
#  customer_id     :integer
#  ride_start_time :datetime
#  ride_end_time   :datetime
#

class Ride < ApplicationRecord
  enum status: [:waiting, :ongoing, :completed]
  belongs_to :customer
  belongs_to :driver, optional: true
end
