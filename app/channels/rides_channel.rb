class RidesChannel < ApplicationCable::Channel
  def subscribed
    stream_from "new_rides"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def new
  end
end
