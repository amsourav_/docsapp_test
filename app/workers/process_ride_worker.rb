class ProcessRideWorker
  include Sidekiq::Worker

  def perform(rideId)
    ride = Ride.find(rideId)
    ride.status = "completed"
    ride.driver.accepted_ride = false
    ride.driver.save! 
    ride.save!
  end
end
