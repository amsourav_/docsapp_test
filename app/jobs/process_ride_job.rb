class ProcessRideJob < ApplicationJob
  queue_as :default

  def perform(ride)
    ride.status = :completed
    ride.driver.accepted_ride = false
    ride.driver.save! 
    ride.save!
    p ride.inspect
  end
end
