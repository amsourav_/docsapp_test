class ApiController < ApplicationController

  before_action :check_driver_id, only: [
                                  :ongoing_rides,
                                  :completed_rides,
                                  :drivers
                                ]

  before_action :check_customer_id, only: :customers
  before_action :set_driver_id, only: :drivers
  before_action :set_customer_id, only: :customers
  before_action :sanitize_accept_ride, only: :accept_ride

  def drivers
    @driver = Driver.find(set_driver_id)

    render json: @driver.to_json(:include => :rides)

  end

  def customers
    @customer = Customer.find(set_customer_id)

    render json: @customer.to_json(:include => :rides)
  end

  def all_rides
    @rides = Ride.all

    render json: @rides
  end

  def new_rides
    @rides = Ride.where(status: :waiting) 

    render json: @rides
  end

  def ongoing_rides
    @rides = Ride.where(status: :ongoing, driver_id: params[:driverId])

    render json: @rides
  end

  def completed_rides
    @rides = Ride.where(status: :completed, driver_id: params[:driverId])

    render json: @rides
  end

  def accept_ride
    @ride = Ride.find(sanitize_accept_ride[:ride_id])
    @driver = Driver.find(sanitize_accept_ride[:driver_id])
    if (@driver.accepted_ride === true)
      render json: {
        message: "Driver already has ride"
      }, status: 422
      return
    end
    if @ride.status === "waiting" && @ride.driver_id == nil
        @ride.driver_id = sanitize_accept_ride[:driver_id]      
        @ride.ride_start_time = Time.now
        @ride.ride_end_time = 5.minutes.from_now
        @ride.driver.accepted_ride = true
        @ride.status = :ongoing
        if (@ride.driver.save! && @ride.save!)
          ProcessRideWorker.perform_in(30.seconds, @ride.id)
          flash[:notice] = "Trip started"
          render json: {
            ride: @ride,
            message: "Ride started"
          }, status: 200
        else
          flash[:error] = "Something went wrong"
          render json: {
            message: "Something went wrong"
          }, status: 422
        end
      else
        flash[:error] = "Something went wrong"
        render json: {
          message: "Something went wrong"
        }, status: 422
      end
  end


  private

  def check_driver_id
    if !params[:driverId]
      render json: {
        message: "Bad Request: Param driverId missing", 
        status: 422 
      }, status: :unprocessable_entity
    else
      true
    end
  end

  def check_customer_id
    if !params[:customerId]
      render json: {
        message: "Bad Request: Param customerId missing", 
        status: 422 
      }, status: :unprocessable_entity
    else
      true
    end
  end

  def set_customer_id
    params[:customerId]
  end

  def set_driver_id
    params[:driverId]
  end

  def sanitize_accept_ride
    params.require(:ride).permit(:customer_id, :driver_id, :ride_id)
  end

end
