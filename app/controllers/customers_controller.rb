class CustomersController < ApplicationController
  before_action :customer_params, only: :create

  def index
    @customer = Customer.new
  end

  def create
    @customer = Customer.find_or_initialize_by(customer_params)
    respond_to do |format|
      @ride = @customer.rides.new
      if @ride.save!
        ActionCable.server.broadcast 'new_rides', @ride 
        format.html { redirect_to customers_path, notice: 'Ride was successfully requested' }
        format.json {
          render json: @ride
        }
      else
        format.html { redirect_to customers_path, error: 'Could not request ride' }
      end
    end
  end


  private

  def customer_params
    params.require(:customer).permit(:id)
  end

end
