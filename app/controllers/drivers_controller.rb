class DriversController < ApplicationController
    before_action :set_driver_id, only: :index
    
    def index
        if !set_driver_id
            @driver = Driver.all
            render template: 'drivers/all'
        else
            begin
                @waitingRides = Ride.where(status: :waiting)
                @driver = Driver.includes(:rides).find(set_driver_id)
                @ongoingRides = Ride.where(status: :ongoing, driver_id: set_driver_id)
                @completedRides = Ride.where(status: :completed, driver_id: set_driver_id)
            rescue ActiveRecord::RecordNotFound
                flash[:error] = "No Such driver"
                redirect_to drivers_path
            end
        end
    end


    private

    def set_driver_id
        params[:id]
    end
end
