App.rides = App.cable.subscriptions.create "RidesChannel",
  connected: ->
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    html = """
        <div class="card" id="card-8">
        <div class="card-block">
            <div class="card-title">
                <span class="left">Req Id: #{data.id}</span>
                <span class="right">Customer Id: #{data.customer_id}</span>
            </div>
            <p class="card-text">seconds ago</p>
              <form novalidate="novalidate" class="simple_form ride" action="/api/v1/rides/accept" accept-charset="UTF-8" data-remote="true" method="post"><input name="utf8" type="hidden" value="✓">
                  <input value="#{data.customer_id}" type="hidden" name="ride[customer_id]" id="ride_customer_id">
                  <input value="#{data.driver_id || window.driverId}" type="hidden" name="ride[driver_id]" id="ride_driver_id">
                  <input value="#{data.id}" type="hidden" name="ride[ride_id]" id="ride_ride_id">
                  <input type="submit" name="commit" value="Select" class="btn btn-default btn btn-primary" data-disable-with="Select">
              </form>
            </div>
      </div>
    """
    $('.wait.body').append(html)

  new: ->
    @perform 'new'
