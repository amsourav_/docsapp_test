# Programming Challenge - DocsApp.in 
## Ola Auto Queuing System v1.2

- Stack: Ruby on Rails
- Db: Postgres

ER Diagram
----

![ER Diagram](./images/erd.png)


APIs
------

- POST /api/v1/drivers             api#drivers            params: driverId
- POST /api/v1/customers           api#customers          params: customerId
- POST /api/v1/rides               api#all_rides          params: No params
- POST /api/v1/rides/waiting       api#new_rides          params: driverId
- POST /api/v1/rides/ongoing       api#ongoing_rides      params: driverId
- POST /api/v1/rides/completed     api#completed_rides    params: driverId
- POST /api/v1/rides/accept        api#accept_ride        params: driverId, customerId, rideId


Pages
------

- /drivers?id=1   -   open dashboard user with id = 1
    
    - example: https://infinite-escarpment-73297.herokuapp.com/drivers?id=1

- /customers      -   form to book cabs
    
    - example: https://infinite-escarpment-73297.herokuapp.com/customers


- /dashboard      -   list status of all rides

    - example: https://infinite-escarpment-73297.herokuapp.com/dashboard


Queues
-------

Using `sidekiq` to process rides when 5 mins are completed


Steps to run
--------------

- `brew install ruby`
- Make sure postgres and redis-server are running
- `gem install bundler rails foreman`
- In project root do `bundle install`
- `foreman start`