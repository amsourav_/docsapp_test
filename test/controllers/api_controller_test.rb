require 'test_helper'

class ApiControllerTest < ActionDispatch::IntegrationTest
  test "should get drivers" do
    get api_drivers_url
    assert_response :success
  end

  test "should get customers" do
    get api_customers_url
    assert_response :success
  end

end
