require 'sidekiq/web'

Rails.application.routes.draw do
  mount ActionCable.server => '/cable'
  mount Sidekiq::Web => '/sidekiq'
  get 'api/drivers'

  get 'api/customers'

  root to: 'customers#index'
  get 'customers', to: 'customers#index'
  post 'customers', to: 'customers#create'
  
  get 'drivers', to: 'drivers#index'
  get 'dashboard', to: 'dashboard#index'


  scope '/api' do
    scope '/v1' do
      scope '/drivers' do
        post '/' => 'api#drivers'
      end
      scope '/customers' do
        post '/' => 'api#customers'
      end
      scope '/rides' do
        post '/' => 'api#all_rides'
        post '/waiting' => 'api#new_rides'
        post '/ongoing' => 'api#ongoing_rides'
        post '/completed' => 'api#completed_rides'
        post '/accept' => 'api#accept_ride'
      end
    end
  end

end
